<?php

use yii\db\Migration;

class m161220_213202_post extends Migration {

    public function up() {

        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'text' => $this->text()->notNull()
        ]);
    }

    public function safeDown() {
        $this->dropTable('post');
    }

}

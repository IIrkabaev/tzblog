var notification = (function () {
    var read = function (div) {
        var id = div.attr('id');

        $.ajax({
            url: to_read_notice,
            type: 'GET',
            dataType: 'json',
            data: {id: id},
        })
                .done(function () {
                    console.log("notification read");
                })
                .fail(function () {
                    console.log("notification not read");
                })
                .always(function () {
                    div.removeClass("alert-success").addClass("alert-warning").find(".not_read").hide();
                });

    };
    $(document).on('click', '.not_read', {}, function () {
        read($(this).closest("div.not_list_item"));
    });
})();
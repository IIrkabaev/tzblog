<?php

namespace app\components;

use yii\base\Event;

class NotificationEvent extends Event {

    const DEFAULT_SUPERADMIN_ID = 1;

    /**
     * @var int $from_user_id User id who send notification
     */
    public $from_user_id = self::DEFAULT_SUPERADMIN_ID;

    /**
     * @var int $to_user_id User id who receive notification
     */
    public $to_user_id = self::DEFAULT_SUPERADMIN_ID;

}

<?php

use yii\db\Migration;

class m161220_214014_notification extends Migration {

    public function up() {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'from_user_id' => $this->integer(),
            'to_user_id' => $this->integer(),
            'to_all_users' => $this->smallInteger()->defaultValue(0)->notNull(),
            'subject' => $this->string()->notNull(),
            'text' => $this->text()->notNull()
        ]);

        $this->createIndex(
                'idx-notification-event_id', 'notification', 'event_id'
        );

        $this->addForeignKey(
                'fk-notification-event_id', 'notification', 'event_id', 'event', 'id', 'CASCADE'
        );

        $this->createIndex(
                'idx-notification-from_user_id', 'notification', 'from_user_id'
        );

        $this->addForeignKey(
                'fk-notification-from_user_id', 'notification', 'from_user_id', 'user', 'id', 'CASCADE'
        );

        $this->createIndex(
                'idx-notification-to_user_id', 'notification', 'to_user_id'
        );

        $this->addForeignKey(
                'fk-notification-to_user_id', 'notification', 'to_user_id', 'user', 'id', 'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropTable('notification');
    }

}

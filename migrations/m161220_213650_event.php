<?php

use yii\db\Migration;

class m161220_213650_event extends Migration {

    public function up() {
        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'label' => $this->string()->notNull(),
            'paste_options' => $this->string()
        ]);

        $this->createIndex('idx-event-name', 'event', 'name', true);

        $this->batchInsert('event', ['name', 'label', 'paste_options'], [
            ['sign_up', 'Sign up', '{username},{siteName},{loginPage}'],
            ['new_posr', 'New Post', '{username},{siteName},{postName},{shortText},{postLink}']
        ]);
    }

    public function safeDown() {
        $this->dropTable('event');
    }

}

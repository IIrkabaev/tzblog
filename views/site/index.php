<?php
/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->registerCssFile('css/notification.css');

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">
        <div class="row">
            <?=
            ListView::widget([
                'options' => [
                    'tag' => 'div',
                ],
                'dataProvider' => $notDataProvider,
                'emptyText' => '',
                'itemView' => '_item_list',
                'itemOptions' => [
                    'tag' => false,
                ],
                'summary' => '',
                'layout' => '{items}{pager}',
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'maxButtonCount' => 4,
                    'options' => [
                        'class' => 'pagination col-xs-12'
                    ]
                ],
            ]);
            ?>
        </div>
        <div class="row">
            <?=
            ListView::widget([
                'options' => [
                    'tag' => 'div',
                ],
                'dataProvider' => $postProvider,
                'emptyText' => '',
                'itemView' => '_post',
                'itemOptions' => [
                    'tag' => false,
                ],
                'summary' => '',
                'layout' => '{items}{pager}',
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'maxButtonCount' => 4,
                    'options' => [
                        'class' => 'pagination col-xs-12'
                    ]
                ],
            ]);
            ?>
        </div>
    </div>
</div>
<?php
$this->registerJs('to_read_notice = \'' . \yii\helpers\Url::to(['site/read-notification']) . '\'');
$this->registerJsFile('js/notification.js', ['depends' => 'app\assets\AppAsset']);

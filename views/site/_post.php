<?php

use yii\bootstrap\Html;

echo Html::beginTag('div', ['class' => 'col-lg-4']);

echo Html::tag('h2', $model->name);
echo Html::tag('p', $model->text);

if (!Yii::$app->user->isGuest)
    echo Html::a('Дaлее', ['site/view', 'id' => $model->id], ['class' => 'btn btn-default']);

echo Html::endTag('div');


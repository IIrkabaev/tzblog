<?php

namespace app\models;

use webvimark\modules\UserManagement\models\User as BaseUser;
use app\components\NotificationBehavior;
use app\components\NotificationEvent;
use Yii;

class User extends BaseUser {

    const EVENT_SIGN_UP = "sign_up";

    public function behaviors() {
        return [
            NotificationBehavior::className(),
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->trigger(self::EVENT_SIGN_UP, new NotificationEvent([
                'to_user_id' => $this->id,
            ]));
        }
    }

}

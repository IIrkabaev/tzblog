<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SentNotification;
use yii\data\ActiveDataProvider;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $notDataProvider = new ActiveDataProvider([
            'query' => SentNotification::find()
                    ->where(['to_user_id' => Yii::$app->user->getId()])
                    ->orderBy('sent_at DESC'),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $postProvider = new \yii\data\ArrayDataProvider([
            'allModels' => \app\models\Post::find()->all(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
                    'notDataProvider' => $notDataProvider,
                    'postProvider' => $postProvider
        ]);
    }

    public function actionView($id) {
        return $this->render('viewpost', ['model' => \app\models\Post::findOne($id)]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionReadNotification($id) {
        /**
         * @var $notification SentNotification
         */
        $notification = SentNotification::find()
                ->where(['id' => $id, 'to_user_id' => Yii::$app->user->getId()])
                ->one();
        $notification->is_read = 1;
        return $notification->save();
    }

}

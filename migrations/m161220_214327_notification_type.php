<?php

use yii\db\Migration;

class m161220_214327_notification_type extends Migration {

    public function up() {
        $this->createTable('notification_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

        $this->batchInsert('notification_type', ['name'], [
            ['Email'],
            ['Browser']
        ]);
    }

    public function safeDown() {
        $this->dropTable('notification_type');
    }

}
